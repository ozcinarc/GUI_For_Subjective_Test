import json, io
from random import shuffle
import os
import pdb

input_json = '/media/koustav/Naihati/Dataset/AVA/AVAComments/CLEAN_AVA_FULL_SCORES_ADDED_MIN_2_CAPTIONS_EACH_CATEGORY.json'

objectivity_threshold = 20
f_1 = io.open('Example_json.json','w', encoding = 'utf-8')
f_2 = io.open('Demo_json.json','w', encoding = 'utf-8')
f_3 = io.open('Exp_1.json','w', encoding = 'utf-8')
sentid = 0

#f_4 = io.open('Exp_2.json','w', encoding = 'utf-8')

#pdb.set_trace()
db = json.load(io.open(input_json, encoding = 'utf-8'))
#import pdb;pdb.set_trace()
for img in db['images']:
    for sent in img['sentences']:
        sent['sentid'] = str(img['imgid']) + '_' + str(sentid)
        sentid+=1
imgs = [img for img in db['images'] if img['split'] != 'train']
#val_imgs = [img for img in imgs if img['split'] == 'val' and len(img['sentences'])>=4]
indices = range(len(imgs)); shuffle(indices); indices = indices[0:60]
for c,i in enumerate(indices):
    if c < 10:
        cmd = 'cp /media/koustav/Naihati/Dataset/AVA+PCCD_Images/' + str(imgs[i]['imgid']) + '.jpg ' +'static/demo/'
    elif c < 35:
        cmd = 'cp /media/koustav/Naihati/Dataset/AVA+PCCD_Images/' + str(imgs[i]['imgid']) + '.jpg ' +'static/experiment1/'
    elif c < 60:
        cmd = 'cp /media/koustav/Naihati/Dataset/AVA+PCCD_Images/' + str(imgs[i]['imgid']) + '.jpg ' +'static/experiment2/'
    os.system(cmd)
display_imgs = []
demo_imgs = []
exp_1_imgs = []
#exp_2_imgs = []

for i in indices[0:5]:
    this_img = {}
    this_img['id'] = imgs[i]['imgid']
    good_sent = [sent for sent in imgs[i]['sentences'] if sent['score'] > objectivity_threshold ];shuffle(good_sent)
    bad_sent = [sent for sent in imgs[i]['sentences'] if sent['score'] <= objectivity_threshold ];shuffle(bad_sent)
    this_img['sentences'] = good_sent[0:2]+bad_sent[0:2]
    display_imgs.append(this_img)
    
for i in indices[5:10]:
    this_img = {}
    this_img['id'] = imgs[i]['imgid']
    good_sent = [sent for sent in imgs[i]['sentences'] if sent['score'] > objectivity_threshold ];shuffle(good_sent)
    bad_sent = [sent for sent in imgs[i]['sentences'] if sent['score'] <= objectivity_threshold ];shuffle(bad_sent)
    this_img['sentences'] = good_sent[0:2]+bad_sent[0:2]
    demo_imgs.append(this_img)
    
for i in indices[10:35]:
    this_img = {}
    this_img['id'] = imgs[i]['imgid']
    good_sent = [sent for sent in imgs[i]['sentences'] if sent['score'] > objectivity_threshold ];shuffle(good_sent)
    bad_sent = [sent for sent in imgs[i]['sentences'] if sent['score'] <= objectivity_threshold ];shuffle(bad_sent)
    this_img['sentences'] = good_sent[0:2]+bad_sent[0:2]
    exp_1_imgs.append(this_img)
    
'''for i in indices[35:60]:
    this_img = {}
    this_img['id'] = imgs[i]['imgid']
    good_sent = [sent for sent in imgs[i]['sentences'] if sent['score'] > objectivity_threshold ];shuffle(good_sent)
    bad_sent = [sent for sent in imgs[i]['sentences'] if sent['score'] <= objectivity_threshold ];shuffle(bad_sent)
    this_img['sentences'] = good_sent[0:2]+bad_sent[0:2]
    exp_2_imgs.append(this_img)'''
    
'''demo_imgs = [{'id':val_imgs[i]['imgid'],'sentences':{
val_imgs[i]['sentences'][0]['sentid']:val_imgs[i]['sentences'][0]['clean'],\
val_imgs[i]['sentences'][1]['sentid']:val_imgs[i]['sentences'][1]['clean'],\
val_imgs[i]['sentences'][2]['sentid']:val_imgs[i]['sentences'][2]['clean'],\
val_imgs[i]['sentences'][3]['sentid']:val_imgs[i]['sentences'][3]['clean']}} for i in indices[0:5]]


exp_1_imgs = [{'id':val_imgs[i]['imgid'],'sentences':{
val_imgs[i]['sentences'][0]['sentid']:val_imgs[i]['sentences'][0]['clean'],\
val_imgs[i]['sentences'][1]['sentid']:val_imgs[i]['sentences'][1]['clean'],\
val_imgs[i]['sentences'][2]['sentid']:val_imgs[i]['sentences'][2]['clean'],\
val_imgs[i]['sentences'][3]['sentid']:val_imgs[i]['sentences'][3]['clean']}} for i in indices[5:30]]


exp_2_imgs = [{'id':val_imgs[i]['imgid'],'sentences':{
val_imgs[i]['sentences'][0]['sentid']:val_imgs[i]['sentences'][0]['clean'],\
val_imgs[i]['sentences'][1]['sentid']:val_imgs[i]['sentences'][1]['clean'],\
val_imgs[i]['sentences'][2]['sentid']:val_imgs[i]['sentences'][2]['clean'],\
val_imgs[i]['sentences'][3]['sentid']:val_imgs[i]['sentences'][3]['clean']}} for i in indices[30:55]]'''


f_1.write(unicode(json.dumps({'Type': 'Display','images': display_imgs}, ensure_ascii=False)))
f_2.write(unicode(json.dumps({'Type': 'Demo','images': demo_imgs}, ensure_ascii=False)))
f_3.write(unicode(json.dumps({'Type': 'Exp_1','images': exp_1_imgs}, ensure_ascii=False)))
#f_4.write(unicode(json.dumps({'Type': 'Exp_2','images': exp_2_imgs}, ensure_ascii=False)))
