from __future__ import print_function
import os
import pdb
#import sys
import numpy as np
import matplotlib
matplotlib.rcParams.update({'font.size': 50})
import matplotlib.pyplot as plt
from sklearn.metrics import confusion_matrix
from sklearn.utils.multiclass import unique_labels
from collections import Counter
import csv
from collections import OrderedDict
import operator
import io
import os


root_folder = 'Results/'
#pdb.set_trace()
all_files = os.listdir(root_folder)

def plot_confusion_matrix(y_true, y_pred, classes,
                          normalize=False,
                          title=None,
                          cmap=plt.cm.Greens):
    """
    This function prints and plots the confusion matrix.
    Normalization can be applied by setting `normalize=True`.
    """
    if not title:
        if normalize:
            title = 'Normalized confusion matrix'
        else:
            title = 'Confusion matrix, without normalization'

    # Compute confusion matrix
    cm = confusion_matrix(y_true, y_pred)
    # Only use the labels that appear in the data
    classes = classes[unique_labels(y_true, y_pred)]
    if normalize:
        cm = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]
        print("Normalized confusion matrix")
    else:
        print('Confusion matrix, without normalization')

    print(cm)

    fig, ax = plt.subplots()
    im = ax.imshow(cm, interpolation='nearest', cmap=cmap)
    #ax.figure.colorbar(im, ax=ax)
    # We want to show all ticks...
    ax.set(xticks=np.arange(cm.shape[1]),
           yticks=np.arange(cm.shape[0]),
           # ... and label them with the respective list entries
           xticklabels=classes, yticklabels=classes
           #title=title,
           #ylabel='True label',
           #xlabel='Predicted label')
           )
    # Rotate the tick labels and set their alignment.
    plt.setp(ax.get_xticklabels(), rotation=0, ha="right",
             rotation_mode="anchor")
    plt.setp(ax.get_yticklabels(), rotation=90, ha="right",
             rotation_mode="anchor")
    # Loop over data dimensions and create text annotations.
    fmt = '.2f' if normalize else 'd'
    thresh = cm.max() / 2.
    for i in range(cm.shape[0]):
        for j in range(cm.shape[1]):
            ax.text(j, i, format(cm[i, j], fmt),
                    ha="center", va="center",
                    color="white" if cm[i, j] > thresh else "black")
    fig.tight_layout()
    return ax

def read_from_file(f_name):
    with open (root_folder + '/' + f_name, 'r') as f:
        data = []
        for line in f:
            line = line.strip('\n')      
            data.append(line.split('#'))
    return data

def compute_exp_1(list_of_files,tag):
    data = None
    
    def average_of_trials(data):
        #pdb.set_trace()
        ui_dict = {}
        gt_dict = {}
        for d in data:
            #pdb.set_trace()
            if ui_dict.get(d[1], None) is None:
                ui_dict[d[1]] = [float(d[3])]
            else:
                ui_dict[d[1]].append(float(d[3]))                

            if gt_dict.get(d[1], None) is None:
                gt_dict[d[1]] = float(d[4])              
            else:
                continue     
        #pdb.set_trace()
        ui_avg_dict = dict([(i,np.mean(j)) for i,j in ui_dict.items()])
        return gt_dict, ui_avg_dict

    #types = ['S15', 'S15', 'S15', 'f8', 'f8']
    #pdb.set_trace()
    for f in list_of_files:
        if not data:
            data = read_from_file(f)
        else:
            data+=read_from_file(f)
    
    gt_dict, ui_dict = average_of_trials(data)
    y_true = [];y_pred = []
    for key in gt_dict.keys():
        y_true.append(int(gt_dict[key] > 20))
        y_pred.append(int(ui_dict[key] >= 50))
    #pdb.set_trace()
    plot_confusion_matrix(y_true, y_pred, classes=np.array(['Bad', 'Good']), normalize = True, \
    title='Confusion matrix, without normalization')
    #plt.savefig(tag)
    #plt.show()
    #pdb.set_trace()
    
def compute_exp_2(list_of_files, tag):
    data = None
    #pdb.set_trace()
    def average_of_trials(data):
        #pdb.set_trace()
        ui_dict = {}
        gt_dict = {}
        caption_dict = {}
        for d in data:
            #pdb.set_trace()
            if ui_dict.get(d[1], None) is None:
                ui_dict[d[1]] = [float(d[3])]
                caption_dict[d[1]] = [unicode(d[2])]
            else:
                ui_dict[d[1]].append(float(d[3]))                

            if gt_dict.get(d[1], None) is None:
                gt_dict[d[1]] = float(d[4])              
            else:
                continue     
        #pdb.set_trace()
        ui_avg_dict = dict([(i,np.mean(j)) for i,j in ui_dict.items()])
        #pdb.set_trace()
        return gt_dict, ui_avg_dict, caption_dict
        
    def prepare_for_supplementary(ui_dict,caption_dict):
        f = io.open('Supplementary_Results_For_Qualitative.txt', 'w')
        b1 = dict([(key, value) for key, value in ui_dict.items() if 'B1' in key])
        b2 = dict([(key, value) for key, value in ui_dict.items() if 'B2' in key])
        ours = dict([(key, value) for key, value in ui_dict.items() if 'Ours' in key])
        ours_sorted =  sorted(ours.items(), key=operator.itemgetter(1), reverse = True)
        #pdb.set_trace()
        for key, value in ours_sorted:
            #pdb.set_trace()
            imid =unicode(key.split('_')[0])
            os.system('cp /media/koustav/Naihati/Dataset/AVA+PCCD_Images/'+\
            imid+'.jpg /home/koustav/Dropbox/My_Work/Research/TCD/Study_Group/Mukta/reading/Neural_Critic/Figures/iccv_19/Supplementary/Qual/' )            
            print(imid, file = f)
            print(caption_dict[imid + '_B1'][0], unicode(b1[imid + '_B1']), file = f, sep = u'#')
            print(caption_dict[imid + '_B2'][0], unicode(b2[imid + '_B2']), file = f, sep = u'#')
            print(caption_dict[imid + '_Ours'][0], unicode(ours[imid + '_Ours']), file = f, sep = u'#')
        
        #pdb.set_trace()
    def compute_good_common_bad(values, tag):
        #pdb.set_trace()
        bins = [0.0, 33.0, 66.0, 100.0]
        percentage = Counter(np.digitize(values, bins = bins))
        '''results = {'A_ID':tag, 'Good':float(percentage[2])/len(values)*100,\
        'Common':float(percentage[1])/len(values)*100,\
        'Bad':float(percentage[0])/len(values)*100,\
        'Score': float(percentage[2])/len(values) *3 + \
        float(percentage[1])/len(values)*2 + \
        float(percentage[0])/len(values)*1}'''
        
        results = {'A_ID':tag, 'Good':float(percentage[3])/len(values)*100,\
        'Common':float(percentage[2])/len(values)*100,\
        'Bad':float(percentage[1])/len(values)*100,\
        'Score': np.sum([key * float(value)/len(values) for key,value in percentage.items()])}
        return results

    #types = ['S15', 'S15', 'S15', 'f8', 'f8']
    #pdb.set_trace()
    for f in list_of_files:
        if not data:
            data = read_from_file(f)
        else:
            data+=read_from_file(f)
    
    gt_dict, ui_dict, caption_dict = average_of_trials(data)
    prepare_for_supplementary(ui_dict, caption_dict)
    #bins = [0, 50, 100]
    #pdb.set_trace()
    b1_results = compute_good_common_bad([value for key, value in ui_dict.items() if 'B1' in key], 'B1')
    #pdb.set_trace()
    b2_results = compute_good_common_bad([value for key, value in ui_dict.items() if 'B2' in key], 'B2')
    our_results = compute_good_common_bad([value for key, value in ui_dict.items() if 'Ours' in key], 'Ours')
    #gt_results =  compute_good_common_bad([value for key, value in ui_dict.items() if 'GT' in key], 'GT')
    #pdb.set_trace()

    #pdb.set_trace()
    return b1_results, b2_results, our_results

exp_1_files = [f for f in all_files if 'experiment_1' in f]
exp_1_files_expert = [f for f in exp_1_files if 'expert' in f]
exp_1_files_non_expert = [f for f in exp_1_files if 'novice' in f  or 'intermediate' in f]

exp_2_files = [f for f in all_files if 'experiment_2' in f]
exp_2_files_expert = [f for f in exp_2_files if 'expert' in f ]
exp_2_files_non_expert = [f for f in exp_2_files if 'novice' in f or 'intermediate' in f]


compute_exp_1(exp_1_files_expert,   'Analysis/Exp_1_Expert.png')
compute_exp_1(exp_1_files_non_expert,  'Analysis/Exp_1_Non_Expert.png')
#b1_results_expert, b2_results_expert, our_results_expert\
#  = compute_exp_2(exp_2_files_expert, 'Exp_2')
#pdb.set_trace()
b1_results_non_expert, b2_results_non_expert, our_results_non_expert =\
 compute_exp_2(exp_2_files_non_expert, 'Exp_2')
 
'''with open('Analysis/Exp_2_Results.csv', 'a') as csvfile:
    scorers = b1_results_expert.keys()
    writer = csv.DictWriter(csvfile, fieldnames=scorers)
    writer.writeheader()

    #writer.writerow(gt_results_expert)
    writer.writerow(b1_results_expert)
    writer.writerow(b2_results_expert)
    writer.writerow(our_results_expert)
    
    #writer.writerow(gt_results_non_expert)
    writer.writerow(b1_results_non_expert)
    writer.writerow(b2_results_non_expert)
    writer.writerow(our_results_non_expert)'''    
#pdb.set_trace()




