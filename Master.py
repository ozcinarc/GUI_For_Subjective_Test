from flask import Flask, request, render_template
import json, io
import csv
from random import shuffle
import numpy as np
#import pdb
#import torch

app = Flask(__name__)

def prepare_images_for_vis(json, which_experiment):
    global sentid;
    #bins = np.array([0,5,10,15,40, 80, 100])
    list_of_images=[]
    for img in json:
        #pdb.set_trace()
        caps = []
        for sent in img['sentences']:                
            caps.append((sent['sentid'], sent['clean'], sent['score']))
            #caps.append((sent['sentid'], sent['clean'], bins[min(np.digitize(sent['score'], bins).item(), len(bins)-1)]))
        #caps = [(sent['sentid'], sent['clean'], bins[np.digitize(sent['score'], bins).item()]) for sent in img['sentences']]
        #caps = [(sent['sentid'], sent['clean'], sent['score']) for sent in img['sentences']]
        if which_experiment in ['example','demo']: 
            list_of_images+=zip(['demo/'+str(img['id'])]*4,caps)
        elif which_experiment == 'experiment1':
            list_of_images+=zip(['experiment1/'+str(img['id'])]*4,caps)            
        elif which_experiment == 'experiment2':
            list_of_images+=zip(['experiment2/'+str(img['id'])]*4,caps)                        
    #pdb.set_trace()
    return list_of_images

example_json = prepare_images_for_vis(json.load(io.open('Example_json.json', encoding = 'utf-8'))['images'], 'example');shuffle(example_json)
demo_json = prepare_images_for_vis(json.load(io.open('Demo_json.json', encoding = 'utf-8'))['images'], 'demo');shuffle(demo_json)
exp_1_json = prepare_images_for_vis(json.load(io.open('Exp_1.json', encoding = 'utf-8'))['images'], 'experiment1');
#pdb.set_trace()
shuffle(exp_1_json);
exp_2_json = prepare_images_for_vis(json.load(io.open('Exp_2.json', encoding = 'utf-8'))['images'], 'experiment2');
#pdb.set_trace()
shuffle(exp_2_json);
#pdb.set_trace()
vis_count = 0
n_example_images = 20
n_demo_images = 20
n_exp_images_1 = 100
n_exp_images_2 = 75
annotations = []
#import pdb;pdb.set_trace()
@app.route("/")
def hello():
    global vis_count, annotations
    vis_count = 0
    annotations = []
    return render_template('index.html')
    display_next_exp_stimuli

@app.route("/give_directions", methods=['POST'])
def give_directions():
    global participant_id, expertise, experiment;
    participant_id = request.form['participant_id']
    expertise = request.form['expertise']
    experiment = request.form['experiment']
    #import pdb;pdb.set_trace()
    print(participant_id, expertise, experiment)
    return render_template('Example_Basic.html', image_caption_score = example_json[0])
    
@app.route("/display_next_example_stimuli", methods=['POST'])    
def display_next_example_stimuli():
    global vis_count
    vis_count +=1
    if vis_count < n_example_images:
        return render_template('Example_Basic.html', image_caption_score = example_json[vis_count])
    else:
        vis_count = 0
        return render_template('start_experiment.html')
    
    
@app.route("/show_demo", methods=['POST'])    
def show_demo():
    global vis_count, exp_1_json, exp_2_json, experiment
    if experiment == "experiment_1":
        return render_template('Demo_Basic.html', image_caption = demo_json[0])
    else:
        vis_count = 0
        json = exp_2_json
    return render_template('Exp_Basic.html', image_caption = json[0])          
        

@app.route("/display_next_demo_stimuli", methods=['POST'])    
def display_next_demo_stimuli():
    global vis_count, exp_1_json, exp_2_json, experiment
    vis_count +=1
    if vis_count < n_demo_images:
        return render_template('Demo_Basic.html', image_caption = demo_json[vis_count])
    else:
        vis_count = 0
        #return render_template('start_experiment.html')
        if experiment == "experiment_1":
            json = exp_1_json
        else:
            json = exp_2_json      
        return render_template('Exp_Basic.html', image_caption = json[0])        

    
'''#@app.route("/start_experiment", methods=['POST'])    
def start_experiment():
    global participant_id, expertise, experiment;
    #import pdb; pdb.set_trace()
    if experiment == "experiment_1":
        json = exp_1_json
    else:
        json = exp_2_json      
    return render_template('Demo_Basic.html', image_caption = json[0])'''

@app.route("/display_next_exp_stimuli", methods=['POST'])    
def display_next_exp_stimuli():
    global vis_count, annotations
    #import pdb; pdb.set_trace()
    global participant_id, expertise, experiment;
    if experiment == "experiment_1":
        json = exp_1_json
    else:
        json = exp_2_json
    score = request.form['score_given']
    annotations.append({'imid' : json[vis_count][0], 'sentid' : json[vis_count][1][0], \
    'caption': json[vis_count][1][1], 'ui': score, 'gt' : json[vis_count][1][2]})      
    #pdb.set_trace()
    vis_count +=1
    if (experiment == "experiment_1" and vis_count < n_exp_images_1) or (experiment == "experiment_2" and vis_count < n_exp_images_2):
        return render_template('Exp_Basic.html', image_caption = json[vis_count])
    else:
        vis_count = 0
        with open('Results/'+participant_id + '_' + expertise + '_' + experiment + '.csv', 'w') as f:
            writer = csv.DictWriter(f, fieldnames = ['imid', 'sentid', 'caption', 'ui', 'gt'], delimiter ="#" )
            #writer.writeheader()
            writer.writerows(annotations)            
        #pdb.set_trace()
        return render_template('thank_you.html')
        
        
@app.after_request
def add_header(response):
    """
    Add headers to both force latest IE rendering engine or Chrome Frame,
    and also to cache the rendered page for 10 minutes.
    """
    response.headers['X-UA-Compatible'] = 'IE=Edge,chrome=1'
    response.headers['Cache-Control'] = 'public, max-age=0'
    return response


if __name__ == "__main__":
#    app.run(host= 'localhost',debug = True)
     app.run(host= '0.0.0.0', port =3134, debug = True)
     